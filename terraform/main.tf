provider "aws" {
  profile = "default"
  region  = "us-east-1"
}

resource "aws_key_pair" "example" {
  key_name   = "examplekey"
  public_key = file("C:/Users/deepika/.ssh/id_rsa.pub")
}

resource "aws_security_group" "instance" {
  name = "terraform-instance"

  ingress {
    from_port   = 0
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "example" {
  key_name               = aws_key_pair.example.key_name
  ami                    = "ami-07d9b9ddc6cd8dd30"
  instance_type          = "t2.micro"
  vpc_security_group_ids = [aws_security_group.instance.id]

  connection {
    type        = "ssh"
    user        = "ubuntu"
    private_key = file("C:/Users/deepika/.ssh/id_rsa")
    host        = self.public_ip
  }

  provisioner "remote-exec" {
    inline = [
      "sudo apt-get update",
      "sudo apt-get install -y puppet",
    ]
  }

  provisioner "file" {
    source      = "puppet/."
    destination = "/tmp/puppet"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo cp -r /tmp/puppet/* /puppet/",
      "sudo puppet apply /puppet/init.pp",
    ]
  }

  tags = {
    Name = "terraform-example"
  }
}

resource "aws_eip" "ip" {
  instance = aws_instance.example.id
}
