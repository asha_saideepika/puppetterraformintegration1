node 'default' {
  package { 'apache2':
    ensure => installed,
  }

  file { '/var/www/html/index.html':
    content => '<html><body><h1>Hello, Puppet!</h1></body></html>',
  }

  service { 'apache2':
    ensure => running,
    enable => true,
  }
}
